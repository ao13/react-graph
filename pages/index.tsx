import Head from 'next/head'
import styles from '../src/styles/Home.module.css'
import * as React from "react"
import TodoContainer from "../src/features/Todo/TodoContainer"

const Home = () =>
    <div className={styles.container}>
        <Head>
            <title>Todos GraphQL</title>
            <link rel='icon' href='/favicon.ico'/>
        </Head>
        <h1>Todo list</h1>
        <TodoContainer/>
    </div>


export default Home
