import type, { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import '../src/styles/globals.css'
import * as React from "react"
import { client } from '../src/api/client'

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  )
}

export default MyApp
