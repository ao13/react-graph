import React from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'

const StyledCheckbox = styled.input.attrs({
    type: 'checkbox'
})`
  padding: 5px;
`

const Checkbox = ({
   onChange = f => f,
   value
}) => {
    return (
        <StyledCheckbox
            onChange={onChange}
            checked={value}
        />
    )
}

Checkbox.propTypes = {
    value: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired
}

export default Checkbox
