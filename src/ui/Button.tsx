import React from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'

const StyledButton = styled.button`
  padding: 5px;
  margin-left: ${props => props.ml + 'rem'};
`

const Button = ({
    children,
    ...rest
}) => {
    return (
        <StyledButton {...rest}>
            {children}
        </StyledButton>
    )
}

Button.propTypes = {
    children: PropTypes.string.isRequired,
}

export default Button
