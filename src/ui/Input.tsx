import React from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'

const StyledInput = styled.input`
  padding: 5px;
`

const Input = ({
   placeholder = '',
   onChange = f => f,
   value = ''
}) => {
    return (
        <StyledInput
            placeholder={placeholder}
            onChange={onChange}
            value={value}
        />
    )
}

Input.propTypes = {
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
}

export default Input
