import {useQuery, useMutation} from "@apollo/client"
import {useEffect, useState} from "react"
import * as React from "react"
import Input from "../../ui/Input"
import Button from "../../ui/Button"
import styled from 'styled-components'
import Checkbox from "../../ui/Checkbox"
import TodoItem from "./TodoItem"
import { GET_TODOS, ADD_TODO, UPDATE_TODO, DELETE_TODO } from "../../graphql/queries"

const StyledTodoContainer = styled.div`
  margin-top: ${props => props.mt + 'rem'}
`

const TodoContainer = () => {
    const { data } = useQuery(GET_TODOS)
    const [todos, setTodos] = useState([])
    const [addTodo] = useMutation(ADD_TODO)
    const [removeTodo] = useMutation(DELETE_TODO)
    const [updateTodo] = useMutation(UPDATE_TODO)

    const [todoText, setTodoText] = useState('')

    const createTodo = (todoText: string): void => {
        if (todoText) {
            addTodo({ variables: { title: todoText } }).then(({ data }) => {
                setTodos(oldTodos => [data.addTodo, ...oldTodos])
            })
            setTodoText('')
        }
    }

    const deleteTodo = (id: string): void => {
        removeTodo({ variables: { id: id }}).then(({ data }) => {
            setTodos(data.removeTodo)
        })
    }

    const editTodo = (id: string): void => {
        updateTodo({ variables: { id: id, completed: true }}).then(({ data }) => {
            setTodos(data.updateTodo)
        })
    }

    useEffect(() => {
        setTodos(data?.getAllTodos ? data?.getAllTodos : [])
    }, [data])

    return (
        <div>
            <Input
                placeholder={'todo'}
                onChange={e => setTodoText(e.target.value) }
                value={todoText}
            />
            <Button onClick={() => createTodo(todoText)}>Add</Button>

            <StyledTodoContainer mt={1}>
                {todos.map(({ id, title, completed}) => (
                    <div key={id}>
                        <TodoItem>{title}</TodoItem>
                        <Checkbox
                            value={completed}
                            onChange={() => editTodo(id)}
                        />
                        <Button onClick={() => deleteTodo(id)} ml={1}>
                            Delete
                        </Button>
                    </div>
                ))}
            </StyledTodoContainer>
        </div>
    )
}

export default TodoContainer
