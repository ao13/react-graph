import * as React from 'react'
import styled from 'styled-components'

const StyledTodoItem = styled.div`
  display: inline-block;
  padding: 5px;
`

const TodoItem = ({ children }) =>
    <StyledTodoItem>
        {children}
    </StyledTodoItem>

export default TodoItem
