import { gql } from '@apollo/client'

export const GET_TODOS = gql`
    query {
        getAllTodos {
            id
            title
            completed
        }
    }
`

export const ADD_TODO = gql`
    mutation addTodo($title: String!) {
        addTodo(todo: {title: $title }) {
            id
            title
            completed
        }
    }
`;

export const DELETE_TODO = gql`
    mutation removeTodo($id: ID!) {
        removeTodo(id: $id) {
            id
            title
            completed
        }
    }
`;

export const UPDATE_TODO = gql`
    mutation updateTodo($id: ID!) {
        updateTodo(id: $id) {
            id
            title
            completed
        }
    }
`;
